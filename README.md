# tucana-user 11 RKQ1.200826.002 22.5.24 release-keys
release-keys
- manufacturer: xiaomi
- platform: sdm660
- codename: whyred
- flavor: tucana-user
- release: 11
- id: RKQ1.200826.002
- incremental: 22.5.24
- tags: release-keys
release-keys
- fingerprint: Xiaomi/dipper/dipper:8.1.0/OPM1.171019.011/V9.5.5.0.OEAMIFA:user/release-keys
Xiaomi/dipper/dipper:8.1.0/OPM1.171019.011/V9.5.5.0.OEAMIFA:user/release-keys
- is_ab: false
- brand: Xiaomi
- branch: tucana-user-11-RKQ1.200826.002-22.5.24-release-keys
release-keys
- repo: xiaomi_whyred_dump
